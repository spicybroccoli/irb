<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/css/style-content.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mp-pusher" class="mp-pusher">
    <nav class="mp-menu" id="mp-menu">
		<div class="mp-level">    	
	        <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>	
                    
        </div>
    </nav>

    <div id="top-menu">
		<a href="#" id="trigger" class="menu-trigger"></a>
	</div>
<header class="header-container">
			<div class="container">
					<div class="logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.jpg" alt=" <?php bloginfo(); ?> "/></a></div>
				<div class="header-container-right">
                <div class="phone hidden-xs">Get in touch: <span> <?php $pages = get_page( 2 ); //GET THE PAGE WITH THE FOOTER CONTACTS 
				//Show the phone
				echo get_post_meta($pages->ID, 'Phone', true); ?>
                </span></div>
				<div class="menu-container">
                <div id="access" role="navigation" class="menu-header">
					<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
				</div><!-- #access -->
                </div>
                </div>
			</div><!-- #container -->
</header><!-- #header -->
		<div id="main">
