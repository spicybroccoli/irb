<?php get_header(); ?>
<div class="slider-container">
<?php 
	// CALL META SLIDER
    echo do_shortcode("[metaslider id=16]"); 
?> </div>
<section class="company-description-container container">

<?php 
// GET ONLY PHONE NUMBER FROM THE CONTENT
$pag = get_page(26); 
echo apply_filters('the_content',$pag->post_content);
?>
</section>
<section class="content-tiles container">

<?php
// ARGS TO SHOW ONLY THE CORRECT PAGES ON THE FRONT PAGE
$args = array(
  'post_type' => 'page',
  'post__in' => array( 11, 9, 7, 6, 5 ), //list of page_ids
  'orderby' => 'menu_order',
  'order' => 'ASC'
);

$page_array = get_posts($args);
//print_r($page_array);
foreach($page_array as $page){
 ?>
 <div class="tiles">
 <div class="tiles-container">
 <div class="tiles-header">
     <h2><a href="<?php echo get_permalink($page->ID); ?>"><?php echo $page->post_title ?></a></h2>
     <div class="tiles-img"><?php echo get_the_post_thumbnail( $page->ID, array( 300, 220),  array( 'class' => 'img-responsive' ) ); ?></div>
 </div>
 <p><?php echo get_post_meta($page->ID, 'Intro Text', true); ?></p>
 <a href="<?php echo get_permalink($page->ID); ?>"> Find out more</a> </div></div>
<?php }// END FOREACH 

// GET THE CONTACT PAGE, THE UNIQUE DIFFERENT PAGE OF THE TILES
$page = get_page( 13 );
?>
  <div class="tiles">
 <div class="tiles-container">
 <div class="tiles-header tile-contact">
 <h2><a href="<?php echo get_permalink($page->ID); ?>"><?php echo get_post_meta($page->ID, 'Text Tile Home', true); ?></a></h2>
 <?php echo get_the_post_thumbnail( $page->ID, array( 300, 392),  array( 'class' => 'img-responsive' )); ?>
 <a href="<?php echo get_permalink($page->ID); ?>"> Contact Us</a>
 </div>
  </div></div>
</section>
<?php get_footer(); ?>