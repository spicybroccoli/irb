<?php
/**
 * Custom Walker for Off Canvas Nav
 *
 * @package Responsive Nav
 */

class SBM_Responsive_Nav extends Walker_Nav_Menu {
	
	function start_lvl(&$output, $depth) {
   		$indent = str_repeat("\t", $depth);
    	$output .= "\n$indent<div class='mp-level'><a href='#' class='mp-back'>back</a><ul>\n";
	}

	function end_lvl(&$output, $depth) {
   		$indent = str_repeat("\t", $depth);
   		$output .= "$indent</ul></div><!--mp-level -->\n";
	}



}

// Load Our Responsive Nav Scripts
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style('responsive-nav-css', get_stylesheet_directory_uri() .'/css/responsive-nav/responsive-nav.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('classie', get_stylesheet_directory_uri() . '/js/responsive-nav/classie.js');
	wp_enqueue_script('mlpushmenu', get_stylesheet_directory_uri() . '/js/responsive-nav/mlpushmenu.js');

});
