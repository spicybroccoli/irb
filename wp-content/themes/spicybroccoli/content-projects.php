<?php
$children = get_pages('child_of='.$post->ID.'&parent='.$post->ID);
//print_r($children);

// VERIFY IF HAS ANY PROJECT or CHILD PAGE INSIDE THE CATEGORY TO SHOW THE PAGE
if(!empty($children)){

// CHECK IF THE CHILDREN HAVE A CHILD TO PRINT OR NOT THE PROJECTS HEADER
// DO A LOOP INSIDE THE CHILDREN ARRAY to VERIFY
foreach($children as $child){
	$projects = get_pages('child_of='.$child->ID);
	if(!empty($projects)){ break; } //BREAK FOREACH IF FOUND CHILD->CHILD PAGE
}

// MAKE SURE THAT THE PROJECT HEADER WONT BE SHOWED IN THE MAIN PAGES
if(empty($projects) AND $post->ID != 5 AND $post->ID != 6 AND $post->ID != 9){
?>
<h3 class="projects-title"><?php echo $post->post_title; ?> Projects</h3>
<?php 
}
 //LOOP O SHOW THE CHILDEN PAGES
 foreach($children as $page){ ?>
 
<div class="tiles">
  <div class="tiles-container tiles-project">
    <div class="tiles-header">
      <h2><a href="<?php echo get_permalink($page->ID); ?>"><?php echo $page->post_title; ?></a></h2>
      <div class="tiles-img"><a href="<?php echo get_permalink($page->ID); ?>">
	  <?php echo get_the_post_thumbnail( $page->ID, array( 300, 220),  array( 'class' => 'img-responsive' )); ?></a></div>
    </div>
  </div>
</div>
<?php } //end foreach
}// end if ?>
