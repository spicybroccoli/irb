	</div><!-- #main -->

	<section  class="content-footer-nav">
    <div class="container">
    <?php wp_nav_menu( array( 'container_class' => 'menu-footer-nav', 'theme_location' => 'primary', 'show_home' => false ) ); ?>
    <footer class="content-footer-contacts">
    <?php $pages = get_page( 2 ) //GET THE PAGE WITH THE FOOTER CONTACTS ?>
    <strong>IRB</strong><br>Carpentry & Property Maintenance
    <div class="phone-footer"><?php echo get_post_meta($pages->ID, 'Phone', true); ?></div>
    <div class="email"><a href="mailto:<?php echo get_post_meta($pages->ID, 'Email', true); ?>"><?php echo get_post_meta($pages->ID, 'Email', true); ?></a></div>
     </footer>
    </div>
</section>
<div  class="content-copy">
<div class="container">
    <div class="copyright">
    Copyright <?php echo date('Y'); ?> IRB. All Rights Reserved.
    </div>
    <div class="sbm">
    Website Design by <em><a href="http://www.spicybroccoli.com/" target="_blank" title="Spicy Broccoli Media">Spicy Broccoli Media</a></em>
    </div>
</div>
</div>

</div>

<?php wp_footer(); ?>
<script>	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>
</body>
</html>
