<?php while ( have_posts() ) : the_post(); ?>
		<?php 
		//CHECK IF HAVE THE IMAGE
		if(get_post_meta(get_the_id(), 'Wide Top Image Url', true) ){?>
            <div class="slider-container-content">
            <img src="<?php echo get_post_meta(get_the_id(), 'Wide Top Image Url', true); ?>" alt="<?php the_title(); ?>"/>
            </div>
        <?PHP } ?>
<div class="container">
        <div id="post-<?php the_ID(); ?>" class="company-description-container ">
			<h1><?php the_title(); ?></h1>


			<div class="entry-content">
				<?php the_content(); ?>
			</div><!-- .entry-content -->
          </div><!-- #post-## -->
		  <?php
			/* GET TEMPLATE TO SHOW GET A QUOTE ON FOOTER */
			 get_template_part( 'content' , 'projects' );
			?>
		
        </div><!-- container -->
<?php endwhile; // End the loop. Whew. ?>